const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const MinifyPlugin = require('babel-minify-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = {
    mode: 'production',
    optimization: {
        minimizer: [new OptimizeCssAssetsPlugin()]
    },
    entry: {
        app: './src/index.js',
        print: './src/print.js',
    },
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './dist',
    },
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].[contentHash].bundle.js',
        publicPath: '/',
    },
    module: {
        rules: [{
                test: /\.html$/,
                use: [{
                    loader: 'html-loader',
                    options: { minimize: false }
                }]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    'babel-loader'
                ]
            },
            {
                test: /\.css$/,
                exclude: /styles\.css$/,
                use: [
                        'style-loader',
                        'css-loader',
                    ]
                    // loader: 'css-loader',
                    // options: {
                    //     modules: {
                    //         exportGlobals: true,
                    //     },
                    // },
            },
            {
                test: /styles\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/i,
                use: [{
                    loader: 'url-loader',
                    options: {
                        // esModule: false,
                        // name: 'assets/[name].[ext]',
                        // outputPath: './assets',
                        // postTransformPublicPath: (p) => `__webpack_public_path__ + ${p}`,
                        // publicPath: '/',
                        // context: 'project',
                    }
                }]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin({ cleanStaleWebpackAssets: false }),
        new MinifyPlugin(),
        // new MiniCssExtractPlugin({
        //     filename: '[name].[contentHash].css',

        //     ignoreOrder: false
        // }),
        new HtmlWebpackPlugin({
            title: 'Development',
            template: './src/index.html',
            filename: './index.html'
        }),
        new HtmlWebpackPlugin({
            title: 'D',
            template: './src/about.html',
            filename: './about.html'
        }),
        new HtmlWebpackPlugin({
            title: 'D',
            template: './src/contact.html',
            filename: './contact.html'
        }),
        new HtmlWebpackPlugin({
            title: 'D',
            template: './src/plays.html',
            filename: './plays.html'
        }),
        new HtmlWebpackPlugin({
            title: 'D',
            template: './src/plays/juego1.html',
            filename: './plays/juego1.html'
        }),
        new HtmlWebpackPlugin({
            title: 'D',
            template: './src/plays/juego2.html',
            filename: './plays/juego2.html'
        }),
        new HtmlWebpackPlugin({
            title: 'D',
            template: './src/plays/juego3.html',
            filename: './plays/juego3.html'
        }),
        new MiniCssExtractPlugin({
            filename: '[name].[contentHash].css',
            ignoreOrder: false
        }),
        new CopyWebpackPlugin([
            { from: './src/assets', to: './assets/' }
        ])
    ],
}